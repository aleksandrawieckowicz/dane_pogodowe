let translations = {
    "dataForCity": "Dane dla miasta",
    "dayData": "Dane z dnia",
    "hour": "godzina",
    "temperature": "Temperatura",
    "degreesCelsius": "ºC",
    "rainSum": "Suma opadów",
    "milimeters": "mm",
    "atmosphericPressure": "Ciśnienie atmosferyczne",
    "hectopascals": "HPa",
    "chooseCity": "WYBIERZ MIASTO",
    "chooseCityInf": "-- Wybierz miasto --",
    "kielce": "KIELCE",
    "krakow": "KRAKÓW",
    "lublin": "LUBLIN",
    "findButton": "WYSZUKAJ",
    "dataSource": "Data-source",
    "iconsSource": "Icons-source",
    "videoSource": "Video-source",

};

const translatePage = () => {
    document.querySelectorAll('[localization-key]').forEach((element) => {
        let key = element.getAttribute('localization-key');
        let translation = translations[key];
        element.innerText = translation;
    });
};

translatePage();