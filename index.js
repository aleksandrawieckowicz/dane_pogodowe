document.querySelector('video').playbackRate = 0.4;

let city = document.getElementById('city');
let date = document.getElementById('date');
let hour = document.getElementById('hour');
let temperature = document.getElementById('temperature');
let rain = document.getElementById('rain');
let pressure = document.getElementById('pressure');

window.addEventListener('load',
    function () {

        if (areMissingFields()) {
            fillEmptyFields();
        } else {
            loadFromStorage();
        }
    },
    false);

function getWeatherDataForCity() {

    let cityNameValue = document.getElementById("citySelect").value;

    if (cityNameValue == "") {
        alert("Wybierz miasto");
        return false;
    }

    fetch('https://danepubliczne.imgw.pl/api/data/synop/station/' + cityNameValue)
        .then((response) => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Nie udało się pobrać danych pogodowych. Spróbuj ponownie później.');
        })
        .then((data) => {

            saveInStorage(data);
            loadFromStorage();

        }).catch((error) => {
            alert(error);
        })
}

function areMissingFields() {
    let dataStorage = getFromLocalStorage();

    return (dataStorage.measuredCity == 'undefined' || dataStorage.measureDate == 'undefined' || dataStorage.measureHour == 'undefined'
        || dataStorage.temp == 'undefined' || dataStorage.rainSum == 'undefined' || dataStorage.press == 'undefined');
}

function fillEmptyFields() {
    city.innerHTML = "-";
    date.innerHTML = "-";
    hour.innerHTML = "-";
    temperature.innerHTML = "-";
    rain.innerHTML = "-";
    pressure.innerHTML = "-";
}

function saveInStorage(receivedData) {

    localStorage.setItem("measuredCity", receivedData.stacja);
    localStorage.setItem("measureDate", receivedData.data_pomiaru);
    localStorage.setItem("measureHour", receivedData.godzina_pomiaru);
    localStorage.setItem("temp", receivedData.temperatura);
    localStorage.setItem("rainSum", receivedData.suma_opadu);
    localStorage.setItem("press", receivedData.cisnienie);
}

function getFromLocalStorage() {
    return {
        measuredCity: localStorage.getItem("measuredCity"),
        measureDate: localStorage.getItem("measureDate"),
        measureHour: localStorage.getItem("measureHour"),
        temp: localStorage.getItem("temp"),
        rainSum: localStorage.getItem("rainSum"),
        press: localStorage.getItem("press"),
    }
}

function loadFromStorage() {
    let dataStorage = getFromLocalStorage();

    city.innerHTML = dataStorage.measuredCity;
    date.innerHTML = dataStorage.measureDate;
    hour.innerHTML = dataStorage.measureHour;
    temperature.innerHTML = dataStorage.temp;
    rain.innerHTML = dataStorage.rainSum;
    pressure.innerHTML = dataStorage.press;
}






